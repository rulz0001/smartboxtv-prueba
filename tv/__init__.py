# encoding=utf-8
from __future__ import unicode_literals

class DateMatch(object):

    def __init__(self, data):
        self.data = data

    @property
    def partido_id(self):
        return self.data['local']['@nc']

    @property
    def partido_estado(self):
        try:
            return int(self.data['estado']['@id'])
        except TypeError:
            pass
        return 0

    @property
    def partido_equipo_local(self):
        return self.data['local']['@nc']

    @property
    def partido_equipo_visita(self):
        return self.data['visitante']['@nc']

    @property
    def partido_goleslocal(self):
        return self.data['goleslocal']

    @property
    def partido_golesvisitante(self):
        return self.data['golesvisitante']

    @property
    def partido_nomGan(self):
        return self.data['@nomGan']

    @property
    def partido_hora(self):
        return self.data['@hora'][0:5]

    @property
    def partido_fecha(self):
        fecha = self.data['@fecha']
        year = fecha[0:4]
        month = fecha[4:6]
        day = fecha[6:8]
        return day+'-'+month+'-'+year
    

    def __str__(self):
        return 'date:{0.partido_fecha}'.format(self)


