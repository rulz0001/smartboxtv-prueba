from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.conf import settings

from .decorators import decorator_datetime, decorator_donwload_xml
from . import DateMatch

from datetime import datetime
import json, time, xmltodict, urllib2


class HomePageView(View):

    @method_decorator(cache_page(60*1)) #cache por 1 min
    @method_decorator(decorator_datetime(datetime.now().isoformat()))
    def get(self, request, *args, **kwargs):
        result = HttpResponse(
            json.dumps(self.create_json(), indent=4, sort_keys=False),
            content_type = 'application/javascript; charset=utf8'
        )
        return result

    def create_json(self):
        self.start = time.time()
        dicc = {}
        dicc.update({
            #'datetime': datetime.now().isoformat(),
            'completed_in': str(round(time.time()-self.start, 4))+' segundos',
            'c':{
                'd':'e',
            },
            'a':'b'
        })
        return dicc

home = HomePageView.as_view()


class SoccerPageView(View):

    def dispatch(self, request, *args, **kwargs):
        self.start = time.time()
        return super(SoccerPageView, self).dispatch(request, *args, **kwargs)

    @method_decorator(cache_page(60*1)) #cache por 1 min
    @method_decorator(decorator_donwload_xml(url='https://s3-sa-east-1.amazonaws.com/cmpsbtv/matchs.xml'))
    @method_decorator(decorator_datetime(datetime.now().isoformat())) 
    def get(self, request):
        name_file = 'matchs.xml'
        path = 'file://'+settings.STATICFILES_DIRS[0]+'/xml/'+name_file
        result = self.get_json(xml_path=path)
        return HttpResponse(
            json.dumps(result, indent=4, sort_keys=False, ensure_ascii=False),
            content_type = 'application/javascript; charset=utf8'
        )

    def get_json(self, xml_path):
        file = urllib2.urlopen(xml_path)
        data = file.read()
        file.close()

        dicc = {}
        partidos = []

        xml_parse = xmltodict.parse(data)['fixture']['fecha']
        for data in xml_parse:
            for partido in data['partido']:
                date_match = DateMatch(partido)
                if date_match.partido_estado == 2:
                    match_dicc = self.generator_match_dicc(date_match)
                    partidos.append(match_dicc)

        # es para darlo vuelta(no era necesario)
        #lines = sorted(partidos, key=lambda k: k['fecha'], reverse=True) 


        dicc.update({
            # "datetime": datetime.now().isoformat(),
            "completed_in": str(round(time.time()-self.start, 4))+' segundos',
            "partidos": partidos
        })

        return dicc

    def generator_match_dicc(self, date_match):
        partido_dicc = {}
        partido_dicc.update({
            "partido": u"{0} - {1}".format(date_match.partido_equipo_local, date_match.partido_equipo_visita),
            "ganador": u"{0}".format(date_match.partido_nomGan),
            "resultado": u"{0} - {1}".format(date_match.partido_goleslocal, date_match.partido_golesvisitante),
            "fecha": u"{0} {1}".format(date_match.partido_hora, date_match.partido_fecha)
        })
        return partido_dicc
        
soccer = SoccerPageView.as_view()
