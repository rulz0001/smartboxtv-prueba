from django.conf import settings
from django.utils.decorators import available_attrs

from functools import wraps

import urllib2, json


def decorator_donwload_xml(url):
    def wrapper(func):
        def wrapped(*args, **kwds):
            result = func(*args, **kwds)
            f = urllib2.urlopen(url)
            data = f.read()
            name_file = f.geturl().split('/')[len(f.geturl().split('/'))-1]
            f = open(settings.STATICFILES_DIRS[0]+'/xml/'+name_file, 'w')
            f.write(data)
            f.close()
            return result
        return wrapped
    return wrapper

def decorator_datetime(datetime):
    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            response = view_func(request, *args, **kwargs)
            dicc = json.loads(response.content)
            dicc['datetime'] = datetime
            response.content = json.dumps(dicc,indent=4, sort_keys=False, ensure_ascii=False)
            return response
        return _wrapped_view
    return decorator